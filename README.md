# AWS CloudFormation Templates

These AWS CloudFormation templates allow researchers at PNNL to deploy resources in AWS.
These templates provide baseline configurations for environments in data analytics, etc.

## Getting started

Start by setting up the easybutton to build out the template.

```
| Easy Buttons                                                 | Name                                                         | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [![Arch_AWS-CloudFormation_32](./images/Arch_AWS-CloudFormation_32.png)](https://us-west-2.console.aws.amazon.com/cloudformation/home?region=us-west-2#/stacks/create/review?templateURL=https://gitlab.com/pacific-northwest-national-laboratory/research-computing/aws-cloudformation-templates/easybutton-ubuntu-docker-simple-scaling-spotonly.cf.yml&stackName=linux-docker-scaling-spotonly) | Ubuntu Linux Docker Simple Scaling Spot Instances          | Two docker executors, scaling based on simple CPU metrics, only spot<br />**Note:** Actual scaling parameters used in this MVP are just to show how to configure scaling - they are untested with Runner workloads - your can help by contributing your tested scaling parameters in an issue. |
```
